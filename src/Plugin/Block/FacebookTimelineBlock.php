<?php

namespace Drupal\jasm\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\jasm\Entity\JasmServiceEntity;

/**
 * Provides a 'FacebookTimelineBlock' block.
 *
 * @Block(
 *  id = "jasm_facebook_timeline",
 *  admin_label = @Translation("Facebook timeline"),
 * )
 */
class FacebookTimelineBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = [
      //      'facebook_app_id'        => '',
      //      'facebook_window_width'  => 300,
      'facebook_window_height' => 500,
    ];

    // Load all the JASM service entities
    $jasm_services = JasmServiceEntity::loadMultiple();

    // Check if any has a "Facebook" preset and use those as defaults
    foreach ($jasm_services as $jasm_service) {
      if ($jasm_service->get('preset') == 'facebook') {
        $configuration['facebook_page_url'] = $jasm_service->get('link');
      }
    }

    return $configuration + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['facebook_page_url'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Page URL'),
      '#description'   => $this->t('The full URL to the page on Facebook, including https://facebook.com.'),
      '#default_value' => (isset($this->configuration['facebook_page_url']) && !empty($this->configuration['facebook_page_url'])) ? $this->configuration['facebook_page_url'] : NULL,
      '#attributes'    => [
        'placeholder'  => $this->t('E.g. "https://www.facebook.com/Rogerwilco.digital/"'),
      ],
      '#required'      => TRUE,
    ];

    $form['facebook_app_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('App ID'),
      '#description'   => $this->t('Provide the Facebook Application ID number. Get this from your Facebook developers platform'),
      '#default_value' => (isset($this->configuration['facebook_app_id']) && !empty($this->configuration['facebook_app_id'])) ? $this->configuration['facebook_app_id'] : NULL,
      '#weight'        => '0',
    ];

    $form['facebook_window_width'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Width'),
      '#description'   => $this->t('Provide the width (in pixels) the timeline widget should display as'),
      '#default_value' => (isset($this->configuration['facebook_window_width']) && !empty($this->configuration['facebook_window_width'])) ? $this->configuration['facebook_window_width'] : NULL,
      '#field_suffix'  => 'px',
      '#weight'        => '3',
    ];

    $form['facebook_window_height'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Height'),
      '#description'   => $this->t('Provide the height (in pixels) the timeline widget should display as'),
      '#default_value' => (isset($this->configuration['facebook_window_height']) && !empty($this->configuration['facebook_window_height'])) ? $this->configuration['facebook_window_height'] : NULL,
      '#field_suffix'  => 'px',
      '#weight'        => '5',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['facebook_page_url'] = $form_state->getValue('facebook_page_url');
    $this->configuration['facebook_app_id'] = $form_state->getValue('facebook_app_id');
    $this->configuration['facebook_window_width'] = $form_state->getValue('facebook_window_width');
    $this->configuration['facebook_window_height'] = $form_state->getValue('facebook_window_height');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    // @TODO: Add this as a library somehow. Not sure how to inject a template
    //        into the site head after the <body> field that way tho 🤔
    $build['jasm_facebook_sdk'] = [
      '#theme'           => 'jasm_facebook_sdk',
      '#facebook_app_id' => $this->configuration['facebook_app_id'],
    ];

    $build['jasm_facebook_timeline'] = [
      '#theme'             =>'jasm_facebook_timeline',
      '#facebook_page_url' => $this->configuration['facebook_page_url'],
      '#facebook_app_id'   => $this->configuration['facebook_app_id'],
      '#width'             => $this->configuration['facebook_window_width'],
      '#height'            => $this->configuration['facebook_window_height'],
    ];

    return $build;
  }

}
