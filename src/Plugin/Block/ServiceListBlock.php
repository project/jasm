<?php

namespace Drupal\jasm\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\jasm\Entity\JasmServiceEntity;


/**
 * Provides a 'ServiceListBlock' block.
 *
 * @Block(
 *  id = "jasm_service_list_block",
 *  admin_label = @Translation("Follow us"),
 * )
 */
class ServiceListBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
            'inline' => '1',
            'display_label' => '1',
          ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    // Load all the services
    $services = $this::fetch_services();

    $form['inline'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Inline'),
      '#description'   => $this->t('Check this box to display an inline list of services'),
      '#default_value' => $this->configuration['inline'],
      '#weight'        => '1',
    ];

    $form['display_label'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Display services label'),
      '#description'   => $this->t('Check this box to hide/show all services label'),
      '#default_value' => isset($this->configuration['hide_label']) ? $this->configuration['display_label'] : NULL,
      '#weight'        => '1',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['inline'] = $form_state->getValue('inline');
    $this->configuration['display_label'] = $form_state->getValue('display_label');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $services = $this::fetch_services();
    // kint($services);

    // $build['jasm_service_list_block_inline']['#markup'] = '<p>' . $this->configuration['inline']. '</p>';

    $build['jasm_service_list'] = [
      '#theme'      => 'item_list',
      '#type'       => 'ul',
      '#title'      => NULL,
      '#items'      => [],
      '#attributes' => [
        'class'     => ['jasm--service-list']
      ],
      '#attached'   => [
        'library'   => ['jasm/service_list_block'],
      ],
      '#wrapper_attributes' => [
        'class'     => ['jasm--service-list--wrapper'],
      ],
    ];
    
    if ($this->configuration['inline']) {
      $build['jasm_service_list']['#attributes']['class'][] = 'jasm--service-list--inline';
      $build['jasm_service_list']['#attributes']['class'][] = 'list-inline';
    }
  
    if ($this->configuration['display_label']) {
      $build['jasm_service_list']['#attributes']['class'][] = 'jasm--service-list--with-label';
    }
    
    $service_links = [];

    foreach ($services as $service) {
      $url = Url::fromUri(
        $service->get('link'),
        [
          'attributes' => [
            'target'   => $service->get('target'),
            'class'    => [$this::strip_string($service->label())],
          ],
        ]
      );
      $link = Link::fromTextAndUrl($service->label(), $url);

      // $build['jasm_service_list']['#items'][$service->weight] = [
      $service_links[$service->weight] = [
        '#markup' => $link->toString(),
        '#wrapper_attributes' => [
          'class'             => ['jasm--service-list--item', 'jasm--service-list--item--' . $this::strip_string($service->label())]
        ],
      ];
    }

    ksort($service_links);
    
    $build['jasm_service_list']['#items']  = $service_links;

    return $build;
  }

  /**
   * Load all the available services
   *
   */
  private function fetch_services() {
    $services = [];

    foreach (JasmServiceEntity::loadMultiple() as $service) {
      $services[] = $service;
    }

    // @TODO: Sort by weight

    // kint($services);

    return $services;
  }

  /**
   * Strip a name to make a friendly class name
   *
   * @param string $string
   *
   * @return bool|false|string|string[]|null
   */
  private function strip_string($string = '') {
    $string = preg_replace ('/[^\pL\d_]+/u', '-', $string);
    $string = trim ($string, "-");
    $string = iconv ('utf-8', "us-ascii//TRANSLIT", $string);
    $string = strtolower ($string);
    $string = preg_replace ('/[^-a-z0-9_]+/', '', $string);

    return $string;
  }
}
