<?php

namespace Drupal\jasm;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of 3rd party social media service entities.
 */
class JasmServiceEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('JASM service');
    // $header['id'] = $this->t('Machine name');
    $header['link'] = $this->t('URL');
    $header['target'] = $this->t('Target');
    $header['preset'] = $this->t('Preset');
    $header['weight'] = $this->t('Weight');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    // $row['id'] = $entity->id();
    $row['link'] = $entity->link;
    $row['target'] = $entity->target;
    $row['preset'] = $entity->preset;
    $row['weight'] = $entity->weight;

    return $row + parent::buildRow($entity);
  }

}
