<?php

namespace Drupal\jasm\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the 3rd party social media service entity.
 *
 * @ConfigEntityType(
 *   id = "jasm_service",
 *   label = @Translation("3rd party social media service"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\jasm\JasmServiceEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\jasm\Form\JasmServiceEntityForm",
 *       "edit" = "Drupal\jasm\Form\JasmServiceEntityForm",
 *       "delete" = "Drupal\jasm\Form\JasmServiceEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\jasm\JasmServiceEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "jasm_service",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *  config_export = {
 *     "id",
 *     "label",
 *     "link",
 *     "target",
 *     "weight",
 *     "preset"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/jasm_service/{jasm_service}",
 *     "add-form" = "/admin/structure/jasm_service/add",
 *     "edit-form" = "/admin/structure/jasm_service/{jasm_service}/edit",
 *     "delete-form" = "/admin/structure/jasm_service/{jasm_service}/delete",
 *     "collection" = "/admin/structure/jasm_service"
 *   }
 * )
 */
class JasmServiceEntity extends ConfigEntityBase implements JasmServiceEntityInterface {

  /**
   * The Client portal menu item ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The pre-configured supported social media services included by default
   *
   * @var string
   */
  public $preset;

  /**
   * The service instance name
   *
   * @var string
   */
  protected $label;

  /**
   * The specific URL of the service
   *
   * @var string
   */
  public $link;

  /**
   * The window target for the link
   *
   * @var string
   */
  public $target;

  /**
   * The sort order (weight) of the item within the category
   *
   * @var integer
   */
  public $weight;

}
