<?php

namespace Drupal\jasm\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining 3rd party social media service entities.
 */
interface JasmServiceEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
