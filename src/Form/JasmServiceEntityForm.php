<?php

namespace Drupal\jasm\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class JasmServiceEntityForm.
 */
class JasmServiceEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $jasm_service = $this->entity;
  
    $form['preset'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Preset'),
      '#description'   => $this->t('Select the 3rd party social media platform for this service. Select "Custom" if no pre-configured solution exists. This value is used to add classes to the markup when displaying lists/links.'),
      '#default_value' => $jasm_service->preset,
      '#options'       => $this::presets(),
      '#empty_option'  => $this->t('-- pick a preset --'),
      '#required'      => TRUE,
    ];
  
    // @TODO: Figure out how to fill this in from the presets above
    $form['label'] = [
      '#type'          => 'textfield',
      '#title' => $this->t('Link text'),
      '#maxlength' => 50,
      '#default_value' => $jasm_service->label(),
      '#description' => $this->t("The text that should display as a link."),
      '#required' => TRUE,
    ];
  
    $form['id'] = [
      '#type'          => 'machine_name',
      '#default_value' => $jasm_service->id(),
      '#machine_name'  => [
        'exists'       => '\Drupal\jasm\Entity\JasmServiceEntity::load',
      ],
      '#disabled'      => !$jasm_service->isNew(),
    ];
  
    $form['link'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Link'),
      '#description'   => $this->t('The URL to the portal item.'),
      '#default_value' => $jasm_service->link,
      '#attributes'    => [
        'placeholder'  => $this->t('E.g. "https://www.example.com"'),
      ],
      '#required'      => TRUE,
    ];
  
    $form['target'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Target window'),
      '#description'   => $this->t('Select the window/tab the link needs to open in.'),
      '#options'       => [
        '_top'         => $this->t('This window/tab'),
        '_blank'       => $this->t('New window/tab'),
      ],
      '#default_value' => ($jasm_service->target) ? $jasm_service->target : '_blank',
      '#required'      => TRUE,
    ];
  
    $form['weight'] = [
      '#type'          => 'weight',
      '#title'         => $this->t('Sort order'),
      '#default_value' => ($jasm_service->weight) ? $jasm_service->weight : 0,
      '#description'   => $this->t('Pick a sort order for this link in this category. Lower numbers will be listed earlier, while higher numbers will be listed later'),
      '#delta'         => 5,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $jasm_service = $this->entity;
    $status = $jasm_service->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label 3rd party social media service.', [
          '%label' => $jasm_service->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label 3rd party social media service.', [
          '%label' => $jasm_service->label(),
        ]));
    }
    $form_state->setRedirectUrl($jasm_service->toUrl('collection'));
  }

  /**
   * Custom function for generating an array of options for presets
   */
  private function presets() {
    $preset_options = [
      'facebook'    => $this->t('Facebook'),
      'twitter'     => $this->t('Twitter'),
      'instagram'   => $this->t('Instagram'),
      'flickr'      => $this->t('Flickr'),
      'linkedin'    => $this->t('LinkedIn'),
      'vkontakte'   => $this->t('VKontakte'),
      'custom'      => $this->t('Custom'),
    ];
    
    return $preset_options;
  }
}
